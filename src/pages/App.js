import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Layout } from "../components/Layout";
import { Home } from "../components/Home";

export const App = () => {
    return (
        <Layout>
            <Router>
                <Route path="/" exact component={Home} />
            </Router>
        </Layout>
    );
};
